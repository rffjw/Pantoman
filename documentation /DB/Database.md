# Data-Base

## Query

__SQLite__: 
this is our query to add tabels and words

[query](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/DB/Query.java)


## Diagram

__tables__: 
our table are here

[table diagram](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/DB/DB.png)


## Words

__words in group__: 
our words in 4 group

[word](https://gitlab.com/rffjw/Pantoman/tree/master/documentation%20/Words)
