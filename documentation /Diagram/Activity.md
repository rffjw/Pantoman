# Activity Diagram

## Step 1

__draw activity diagram related to player__: 
at this activity we just select team name

[activity step1](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/teamnameActivity.png)

## Step 2

__draw activity diagram related to game condition__: 
now we want to set game condition parallel with enter team name

[activity step2](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/gameconditionActivity.png)

## Step 3

__draw activity diagram for creat objects__: 
creat two object for player and game

[activity step3](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/creatobjectActivity.png)

## Step 4

__draw activity diagram related to words__: 
now user shuold select the word

[activity step4](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/selectwordActivity.png)

## Step 5

__draw activity diagram related to playing__: 
make the timer run player act and at the end the score calculate

[activity step5](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/playingActivity.png)

## Step 6

__draw activity diagram related to score__: 
show score to player

[activity step6](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/scoreActivity.png)

## Final Step

__final activity diagram__: 
finaly, if round is bigger then zero we back to playing if is zero, the game is finish

[activity diagram](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/finalActivity.png)

