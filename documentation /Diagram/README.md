# Diagrams

[USECASE](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/UseCase.md)

[ACTIVITY](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Activity.md)

[SEQUENCE](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Sequence.md)

[CLASS](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Class.md)
