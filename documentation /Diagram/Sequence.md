# Sequence Diagram

## Step 1

__draw sequence diagram related to player and game__: 
start with creat two object and send data to them

[sequence step1](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/nameconditionSequence.png)


## Step 2

__draw sequence diagram related to word__: 
now game send a message to word and word pick a word from database and show it

[sequence step2](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/wordSequnce.png)


## Final Step

__draw final sequence diagram__: 
after player acting we shoud send calculated score to player and at the final round player send win to game and game show winner to user

[sequence diagram](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/finalSequence.png)
