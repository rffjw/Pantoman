# Pantoman Scenario

* __Purpose__: play and learn a complete round.
* __People__: member of two teams play pantoman
* __Equipment__: a PC or laptop


* __Scenario__:
    1. player open the game
    2. each team choose team name
    3. both teams select game configuration(time and round)
    4. firs team start the game
    5. the player choose word
    6. start acting
    7. timer is on
    8. if he/she make it before time goes down click button done
    9. if he/she can't do it next page will be open(true or false page)
    10. player select true or false according to his/her act(successful = true, unsuccessful = false)
    11. scores will be shown
    12. click on title score and the next team continue the game 


# The winner Scenario

* __Purpose__: introduce the winner

* __Scenario__:
    1. after final round 
    2. in the score page
    3. score of each team show next to the team name
    4. the team with higher score is winner
    5. a W appear in front of the winner team 


# Select teams name Scenario

* __Purpose__: select name for teams

* __Scenario__:
    1. in middle the main page there is two filed for team name
    2. the first filed is for startng team
    3. next filed is for the second team
    4. users enter their favorites name
    5. then clik on start button


# Game configuration Scenario

* __Purpose__: configurate game condition

* __Scenario__:
    1. there is two part in main page for game condition
    2. the first time(how many time they need to act a word)(unit = second)
    3. the second round(how many round they want to play game)
    4. teams enter their intended time and round 
    5. then they click on start button


# Score Calculation Scenario

* __Purpose__: claculate team's score

* __Scenario__:
    1. each word has specific score
    2. after acting player say that was true or false
    3. if it was true the score will be add to previous score
    4. if it was false score remain unchange
    5. every 30s has got one more score
    6. scores will be shown in front of team name


# Select words Scenario

* __Purpose__: select a word to act

* __Scenario__:
    1. in words page we got four group of words(general, sport, art, science)
    2. each group has three level(one, two and three score)
    3. player will choose the favorit word
    4. next page(timer) will be open


# The act and timer Scenario

* __Purpose__: run the timer when player act the word

* __Scenario__:
    1. in this page there is a timer
    2. the choosen word will be show on top of the page
    3. player until has time can act
    4. if he finish acting before time goes down, he/she click on done to save tmie
    5. the score of remaining time will be save
    6. score page wil be open
    


