INSERT INTO General
    VALUES (NULL, "Cotton", 3), (NULL, "Sparrow", 3), (NULL, "Careful", 3),
      (NULL, "Respect", 3), (NULL, "Migration", 3), (NULL, "Slip", 2),
      (NULL, "Ceiling", 2), (NULL, "Monkey", 2), (NULL, "Coal", 2),
      (NULL, "Winter", 2), (NULL, "Oil", 1), (NULL, "Full", 1),
      (NULL, "Angry", 1), (NULL, "Farming", 1), (NULL, "Angel", 1);

INSERT INTO Sport
VALUES (NULL, "Cricket", 3), (NULL, "Comeback", 3), (NULL, "Poles", 3),
  (NULL, "Paralympics", 3), (NULL, "Pilates", 3), (NULL, "Striker", 2),
  (NULL, "Stadium", 2), (NULL, "Wrestling", 2), (NULL, "Champion", 2),
  (NULL, "Punch", 2), (NULL, "Tennis", 1), (NULL, "Football", 1),
  (NULL, "Shoot", 1), (NULL, "Save", 1), (NULL, "Golf", 1);

INSERT INTO Art
VALUES (NULL, "Mona Lisa", 3), (NULL, "Surrealism", 3), (NULL, "Kill Bill", 3),
  (NULL, "Artistic", 3), (NULL, "Horror", 3), (NULL, "Director", 2),
  (NULL, "Theater", 2), (NULL, "Drawing", 2), (NULL, "Creative", 2),
  (NULL, "Graphist", 2), (NULL, "Paint", 1), (NULL, "Cinema", 1),
  (NULL, "Brush", 1), (NULL, "Film", 1), (NULL, "Actor", 1);

INSERT INTO Science
VALUES (NULL, "Aftershock", 3), (NULL, "Source", 3), (NULL, "Probe", 3),
  (NULL, "Satellite", 3), (NULL, "Neptune", 3), (NULL, "Microscope", 2),
  (NULL, "Reflection", 2), (NULL, "Scientist", 2), (NULL, "Galaxy", 2),
  (NULL, "Temperature", 2), (NULL, "Region", 1), (NULL, "Solar", 1),
  (NULL, "Professor", 1), (NULL, "Kernel", 1), (NULL, "Client", 1);