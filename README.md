# Pantoman
It's a simple game that make you feel better togehetr!

## Features
* Various and classified words
* Calculate and show scores
* Keep time
* Easy to use

## Documentation

[SCENARIO](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/SCENARIO.md)

[REQUAIERMENT](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/REQUIREMENTS.md)

[FASIBILITY](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/FEASIBILITY.md)

[USECASE](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/UseCase.md)

[ACTIVITYDIAGRAM](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Activity.md)

[SEQUENCEDIAGRAM](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Sequence.md)

[CLASSDIAGRAM](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Class.md)

[WORDS](https://gitlab.com/rffjw/Pantoman/tree/master/documentation%20/Words)

[DATABASE](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/DB/Database.md)

[UI](https://gitlab.com/rffjw/Pantoman/tree/master/documentation%20/UI)

[CODE](https://gitlab.com/rffjw/Pantoman/tree/master/documentation%20/FPantoman)

## Steps

1. Design first page
2. Collect and classify words
3. Complete UI
4. Class implementation
5. Database implementation
6. Check diagrams with code
7. Connect Code to UI
8. Final test
9. Release



## Developers
| Name |   ID     |
|:----------------:|:--------:|
|     Salh Najarzade     | @rffjw |
