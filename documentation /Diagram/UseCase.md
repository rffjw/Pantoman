# Use-Case Diagram

## Step 1

__draw use-case related to player__: 
first of all we draw the select team use-case and its about team names

[use-case step1](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/playerUse-Case.png)

## Step 2

__draw use-case related to game condition__: 
now we want to set game condition and show the relation to set name 

[use-case step2](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/conditionUse-Case.png)

## Step 3

__draw use-case related to select words__: 
after select name and game condition you need to select words

[use-case step3](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/selectWord.png)

## Step 4

__draw use-case related to playing__: 
make the timer run

[use-case step4](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/playingUse-Case.png)

## Step 5

__draw use-case related to score__: 
now we must record and show score to player

[use-case step5](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/scoreUse-Case.png)

## Final Step

__final use-case diagram__: 
finaly

[use-case](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/updateUsecase.jpg)

