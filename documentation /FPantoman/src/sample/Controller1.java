package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller1 implements Initializable {

    @FXML
    public Label WordSelectTeam;
    @FXML
    public Button G3;
    @FXML
    public Button S3;
    @FXML
    public Button A3;
    @FXML
    public Button SC3;
    @FXML
    public Button G2;
    @FXML
    public Button SC2;
    @FXML
    public Button A2;
    @FXML
    public Button S2;
    @FXML
    public Button G1;
    @FXML
    public Button SC1;
    @FXML
    public Button A1;
    @FXML
    public Button S1;


    public static void show() throws IOException {

        Parent root = FXMLLoader.load(Main.class.getResource("sample1.fxml"));
        Stage secondStage = new Stage();
        secondStage.setTitle("PANTOMAN");
        secondStage.setScene(new Scene(root));
        secondStage.show();


    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        WordSelectTeam.setText(Main.P1.TeamName);

        G3.setOnAction(event ->{


            try {

                Main.Word.Word = "Galaxy";
                Main.Word.WordScore = 3;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        G2.setOnAction(event ->{

            try {

                Main.Word.Word = "Lamp";
                Main.Word.WordScore = 2;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        G1.setOnAction(event ->{

            try {

                Main.Word.Word = "Headset";
                Main.Word.WordScore = 1;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        S3.setOnAction(event ->{

            try {

                Main.Word.Word = "Aerobic";
                Main.Word.WordScore = 3;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        S2.setOnAction(event ->{

            try {

                Main.Word.Word = "Heading";
                Main.Word.WordScore = 2;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        S1.setOnAction(event ->{

            try {

                Main.Word.Word = "Workout";
                Main.Word.WordScore = 1;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        A3.setOnAction(event ->{

            try {

                Main.Word.Word = "Kill Bill";
                Main.Word.WordScore = 3;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        A2.setOnAction(event ->{

            try {

                Main.Word.Word = "Actor";
                Main.Word.WordScore = 2;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        A1.setOnAction(event ->{

            try {

                Main.Word.Word = "Colorful";
                Main.Word.WordScore = 1;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        SC3.setOnAction(event ->{

            try {

                Main.Word.Word = "JAVAFX";
                Main.Word.WordScore = 3;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        SC2.setOnAction(event ->{

            try {

                Main.Word.Word = "Steam";
                Main.Word.WordScore = 2;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );

        SC1.setOnAction(event ->{

            try {

                Main.Word.Word = "H2O";
                Main.Word.WordScore = 1;
                Controller2.show1();

            } catch (IOException e) {

            }

        } );


    }
}
