
|  ID |  Words  | Score |
| :-: | :-----: | :---: |
| 1   | Mona Lisa  | 3     |
| 2   | Surrealism  | 3     |
| 3   | Kill Bill  | 3     |
| 4   | Artistic  | 3     |
| 5   | Horror  | 3     |
| 6   | Director  | 2     |
| 7   | Theater  | 2     |
| 8   | Drawing  | 2     |
| 9   | Creative  | 2     |
| 10   | Graphist  | 2     |
| 11   | Paint  | 1     |
| 12   | Cinema  | 1     |
| 13   | Brush  | 1     |
| 14   | Film  | 1     |
| 15   | Actor  | 1     |
