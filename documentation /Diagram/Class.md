# Class Diagram

## Step 1

__pre-diagram__: 
we try to predict and determined the classes, methods and attributes, we have 3 class(Player, Game and Words)

__Player__: attribute(teamName, Score, Win) method(setTeamName)

__Game__: attribute(Time, timeLeft, Round, Result) method(Calculate, timerMusic, setGame)

__Word__: attribute(wordScore, wordType) method(showWord, sendWordScore)

[classes step1](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/ClassDiagram.jpg)


## Final Step

__Class Diagram__: 
and it's the real class diagram

[ClassDiagram](https://gitlab.com/rffjw/Pantoman/blob/master/documentation%20/Diagram/Class.png)