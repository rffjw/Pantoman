package sample;


import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller4 implements Initializable {


    public Button nextround;
    public Label t1;
    public Label s2;
    public Label s1;
    public Label t2;

    public static void show3() throws IOException {

        Parent root = FXMLLoader.load(Main.class.getResource("sample4.fxml"));
        Stage secondStage = new Stage();
        secondStage.setTitle("PANTOMAN");
        secondStage.setScene(new Scene(root));
        secondStage.show();

    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        t1.setText(Main.P1.TeamName);
        s1.setText(String.valueOf(Main.P1.Score));

        t2.setText(Main.P2.TeamName);
        s2.setText(String.valueOf(Main.P2.Score));

         nextround.setOnAction(event ->{

          try {

             Controller1.show();

          } catch (IOException e) {

           }

         } );

    }
}
