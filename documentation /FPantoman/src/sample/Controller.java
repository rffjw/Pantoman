package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    public Button StartGame;
    @FXML
    public TextField GameRound;
    @FXML
    public TextField GameTime;
    @FXML
    public TextField Name2;
    @FXML
    public TextField Name1;



    @Override
    public void initialize(URL location, ResourceBundle resources) {


        StartGame.setOnAction(event ->{

            try {

                Main.Game.startGame(Name1.getText(), Name2.getText(), Integer.parseInt(GameTime.getText()), Integer.parseInt(GameRound.getText()));
                Controller1.show();

            } catch (IOException e) {

            }

        } );

    }
}
