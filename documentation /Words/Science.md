
|  ID |  Words  | Score |
| :-: | :-----: | :---: |
| 1   | Aftershock  | 3     |
| 2   | Source  | 3     |
| 3   | Probe  | 3     |
| 4   | Satellite  | 3     |
| 5   | Neptune  | 3     |
| 6   | Microscope  | 2     |
| 7   | Reflection  | 2     |
| 8   | Scientist  | 2     |
| 9   | Galaxy  | 2     |
| 10   | Temperature  | 2     |
| 11   | Region  | 1     |
| 12   | Solar  | 1     |
| 13   | Professor  | 1     |
| 14   | Kernel  | 1     |
| 15   | Client  | 1     |
