
|  ID |  Words  | Score |
| :-: | :-----: | :---: |
| 1   | Cricket  | 3     |
| 2   | Comeback  | 3     |
| 3   | Poles  | 3     |
| 4   | Paralympics  | 3     |
| 5   | Pilates  | 3     |
| 6   | Striker  | 2     |
| 7   | Stadium  | 2     |
| 8   | Wrestling  | 2     |
| 9   | Champion  | 2     |
| 10   | Punch  | 2     |
| 11   | Tennis  | 1     |
| 12   | Football  | 1     |
| 13   | Shoot  | 1     |
| 14   | Save  | 1     |
| 15   | Golf  | 1     |
