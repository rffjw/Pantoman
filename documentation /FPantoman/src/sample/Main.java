package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main extends Application {

    public static Game Game = new Game();
    public static Player P1 = new Player();
    public static Player P2 = new Player();
    public static Word Word = new Word();


    String connectionString = "jdbc:sqlite:./PData";

    Connection connection = DriverManager.getConnection(connectionString);


    public Main() throws SQLException {

        java.sql.Statement statement = connection.createStatement();
        java.sql.Statement statement1 = connection.createStatement();
        java.sql.Statement statement2 = connection.createStatement();
        java.sql.Statement statement3 = connection.createStatement();

        ResultSet general = statement.executeQuery("SELECT * FROM General");
        ResultSet sport = statement1.executeQuery("SELECT  * FROM  Sport");
        ResultSet art = statement2.executeQuery("SELECT * FROM Art");
        ResultSet science = statement3.executeQuery("SELECT * FROM Science");

        while (general.next())
        Main.Word.loadGWord(general.getString("Words"), general.getInt("Score"));

        while (sport.next())
            Main.Word.loadSWord(sport.getString("Words"), sport.getInt("Score"));

        while (art.next())
            Main.Word.loadAWord(art.getString("Words"), art.getInt("Score"));

        while (science.next())
            Main.Word.loadSCWord(science.getString("Words"), science.getInt("Score"));

    }


    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("PANTOMAN");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }

    public static void main(String[] args) {launch(args);}

}
