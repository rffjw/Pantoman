package sample;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller2 implements Initializable, Game.OnTimeChange {

    @FXML
    public Label timer;
    @FXML
    public Label wordshow;
    @FXML
    public Button done;

    public static void show1() throws IOException {

        Parent root = FXMLLoader.load(Main.class.getResource("sample2.fxml"));
        Stage thirdStage = new Stage();
        thirdStage.setTitle("PANTOMAN");
        thirdStage.setScene(new Scene(root));
        thirdStage.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        wordshow.setText(Main.Word.showWord());
//        timer.setText(Main.game.showTime);

        Main.Game.setTimeChangeListener(this);

            Main.Game.timerMusic(Main.Game.Time);

        done.setOnAction(event ->{

            try {

                Controller3.show2();

            } catch (IOException e) {

            }

        } );

    }

    @Override
    public void onChange(String time) {

        Platform.runLater(() -> timer.setText(time));


    }
}
