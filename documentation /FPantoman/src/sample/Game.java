package sample;

public class Game {

    public int Time;
    public int TimeLeft;
    public String showTime;
    public int Round;
    public boolean Result;

    public int Calculate(int score, boolean win, int wordscore){
        int S = 0;
        if (win == true){
            S = score + wordscore;
        }
        return S;
    }
    public void timerMusic(int time){

        Thread tr=new Thread(()->{
           try{
               for(int i = time; i>0; i--){
                   Thread.sleep(1000);
                   showTime = String.valueOf(i);
                   System.out.println(i);
                    if (this.timeChangeListener!=null){
                        this.timeChangeListener.onChange(showTime);
                    }
            }
           }catch (Exception e){
           }
           timeOut();
        });
        tr.start();
    }
    public interface OnTimeChange{
        void onChange(String time);
    }
    private OnTimeChange timeChangeListener;
    public void setTimeChangeListener(OnTimeChange listener){
        this.timeChangeListener=listener;
    }
    public void timeOut(){
    }


    public void setGameCondition(int itime, int iround){

        Time = itime;
        Round = iround;

    }

    public void startGame(String n1, String n2, int t, int r){

        Main.P1.setTeamName(n1);
        Main.P2.setTeamName(n2);
        Main.Game.setGameCondition(t, r);

    }
}
