package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller3 implements Initializable {


    @FXML
    public Button TT;
    @FXML
    public Button FF;

    public static void show2() throws IOException {

        Parent root = FXMLLoader.load(Main.class.getResource("sample3.fxml"));
        Stage secondStage = new Stage();
        secondStage.setTitle("PANTOMAN");
        secondStage.setScene(new Scene(root));
        secondStage.show();

    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        TT.setOnAction(event ->{

            try {

                Main.Game.Result = true;
                Main.P1.Score = Main.Game.Calculate(Main.P1.Score, Main.Game.Result, Main.Word.WordScore);

                Controller4.show3 ();

            } catch (IOException e) {

            }

        } );

        FF.setOnAction(event ->{

            try {

                Main.Game.Result = false;
                Main.P1.Score = Main.Game.Calculate(Main.P1.Score, Main.Game.Result, Main.Word.WordScore);
                Controller4.show3 ();

            } catch (IOException e) {

            }

        } );

    }

}
